(function(_this){
	var mrpc = {};
	var calls = [];
	var lastCallId = 0;
	var socket;
	var gate;

	function connect (host, port, gateName, callback) {
		socket = new WebSocket("ws://"+ host + ":" + port);
		gate = gateName;

		socket.onmessage = function(event){
			var data = JSON.parse(event.data);

			calls = calls.filter(function(item){
				if (item.id !== data.req.id) return true;
				item.callback(null, data.result);
				return false;
			});
		};

		socket.onopen = function(event){
			callback();
		};

		socket.onclose = function(event){
			calls.forEach(function(item){
				item.callback({name: "ConnectionClosed", message: "connection closed!"}, null);
			});
			calls = [];
		};
	}

	function call (name, params, callback) {
		var id = ++lastCallId;
		calls.push({date: Date.now(), 
				   id: id,
				   gate: gate,
				   callback: callback});
		socket.send(JSON.stringify({id: id, method: name, gate: gate, params: params}));
	}

	setInterval(function(){
		calls = calls.filter(function(item){
			if (Date.now() - item.date < 60 * 1000) return true;
			item.callback({name: "Timeout", message: "Timeout!"}, null);
			return false;
		});
	}, 60 * 1000);

	mrpc.connect = connect;
	mrpc.call = call;
	
	_this.mrpc = mrpc;
})(this);