var config = require("./config.json");
var WebSocketServer = require("ws").Server;
var wss = new WebSocketServer({port: 8081});
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());

var lastClientId = 0;
var lastWorkerId = 0;

var clients = [];
var workers = [];
var requests = []; 

function sendRequestsToWorker (workers, requests) {
	if (workers.length === 0 || requests.length === 0) return;
	var worker = workers.shift();
	var request = requests.shift();

  	worker.res.json(JSON.stringify(request));
}


wss.on("connection", function connection(ws) {
	var id = ++lastClientId;
	clients.push({socket: ws, id: id});
	console.log("Client #" + id + " connected.");

	ws.on("message", function incoming(message) {
		req = JSON.parse(message);

		if (req.gate !== config.gate) {
			ws.close();
			return;
		} 

		requests.push({clientId: id, date: Date.now(), req: req});
		sendRequestsToWorker(workers, requests);
	});

	ws.on("close", function incoming(message) {
		removeClient();	
		console.log("Client #" + id + " disconnected.");
	});

	function removeClient () {
		clients = clients.filter(function(item){
			return item.id !== id;
		});
		requests = requests.filter(function(item){
			return item.clientId !== id;
		});
	}
});


app.get("/gate/:gate/key/:key", function (req, res) {
	if ( !(req.params.gate == config.gate && req.params.key == config.key)) {
		console.log("Bad connect to gate '" + req.params.gate + "'' with key '" + req.params.key + "'");
		res.status(400).send('Bad Request');
		return;
	}


	var id = ++lastWorkerId;

	workers.push({id: id, gate: req.params.gate, res: res});
	console.log("Worker #" + id + " connected.");

	req.on("close", function() {
		removeWorker();
		console.log("Worker #" + id + " disconnected.");
	});

	req.on("end", function() {
		removeWorker();
	});

	sendRequestsToWorker(workers, requests);

	function removeWorker () {
		workers = workers.filter(function(item){
			return item.id !== id;
		});
	}
});


app.post("/gate/:gate/key/:key", function (req, res) {
	if ( !(req.params.gate == config.gate && req.params.key == config.key)) {
		console.log("Bad connect to gate '" + req.params.gate + "'' with key '" + req.params.key + "'.");
		res.status(400).send('Bad Request');
		return;
	}

	clients.forEach(function(item){
		if (req.body.clientId !== item.id) return;
		item.socket.send(JSON.stringify(req.body));
		console.log("Result sended to Client #" + req.body.clientId);
	});

	res.json({"status": "ok"});
});


var server = app.listen(8080, function () {});


setInterval(function(){
	requests = requests.filter(function(item){
		return (Date.now() - item.date < 60 * 1000);
	});
	console.log("Garbage collected.");
}, 60 * 1000);
